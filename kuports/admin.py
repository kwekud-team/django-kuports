from django.contrib import admin 
#
# from tools.abstract.admin import BaseAdmin
# from extend.sortsets.models import DatePreset
# from extend.sortsets.attrs import DatePresetAttr
#
#
# @admin.register(DatePreset)
# class DatePresetAdmin(BaseAdmin):
#     list_display = DatePresetAttr.admin_list_display
#     list_filter = DatePresetAttr.admin_list_filter
#     fieldsets = DatePresetAttr.admin_fieldsets
#     list_editable = ['sort', 'is_active', 'is_default']