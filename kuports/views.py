from django.views.generic import TemplateView
from kdash.views import BaseDashView


class ReportCentralView(BaseDashView):
    template_name = 'kuports/report_central.html'
    page_title = 'Reports'
    page_subtitle = '---'

    def get_context_data(self, **kwargs):
        context = super(ReportCentralView, self).get_context_data(**kwargs)

        context.update({
            'report_section_list': self.get_list(),
        })
        return context

    def get_list(self):
        dt = {
            '': ('printery:printery.printlog-reportlist',
                 'printery:printery.printjob-reportsummary',
                 'printery:printery.printjobactivity-reportlist'),
            # '': ('printery:printery.printlog-reportlist',
            #      'printery:printery.printjob-reportsummary',
            #      'printery:printery.printjobactivity-reportlist'),
        }
        return [
            {'name': 'Print Jobs', 'url': '', 'summary': 'Report showing all print jobs with their status and printed'
                                                         'page count.'}
        ]