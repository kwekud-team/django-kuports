#
#
# class DatePresetAttr(object):
#     admin_list_display = ['name', 'duration_value', 'duration_type',
#     	'note', 'sort', 'is_default', 'is_active']
#     admin_list_filter = ['duration_type', 'is_active']
#     admin_fieldsets = [
#         ('Basic', {'fields': ('name', 'duration_value', 'duration_type',)}),
#         ('Extra', {'fields': ['note', 'sort', 'is_default', 'is_active']}),
#     ]
#     serializer_fields = ['name', 'duration_value', 'duration_type', 'is_default',
#     	'note', 'sort', 'is_active']