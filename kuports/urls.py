from django.urls import path

from extend.kuports import views


app_name = 'kuports'


urlpatterns = [
    path('central/printing/', views.ReportCentralView.as_view(), name='report_central_printing'),
]
